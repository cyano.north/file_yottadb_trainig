package main

import (
	"fmt"
	"lang.yottadb.com/go/yottadb"
	"os"
)

const callTableLoc = "/home/dev1/go/yotta/src/trainYotta/callM/calltab.ci"
const routineLoc = "/ydbdir/rtns/"

func main() {
	var mrtn yottadb.CallMDesc
	defer mrtn.Free()

	// setup environment
	err := os.Setenv("ydb_ci", callTableLoc)
	if err != nil {
		fmt.Println("Can set ydb_ci")
	}

	//test call M function
	fmt.Println("Golang: Invoking M - RUN^ZNEWTEST (Run), use linetag, param, return")
	mrtn.SetRtnName("Run")
	retval, err := mrtn.CallMDescT(yottadb.NOTTP, nil, 64, "KEY1", "VALUE")
	if nil != err {
		panic(fmt.Sprintf("CallMDescT() call failed: %s", err))
	}
	fmt.Println("Golang: retval =", retval)
	fmt.Println("ASCII code of first character in return string = ", int(retval[0]))

	fmt.Println("Golang: Invoking M - RUNNOPARAM^ZNEWTEST (RunNoParam), use linetag, no param, return")
	mrtn.SetRtnName("RunNoParam")
	retval, err = mrtn.CallMDescT(yottadb.NOTTP, nil, 64)
	if nil != err {
		panic(fmt.Sprintf("CallMDescT() call failed: %s", err))
	}
	fmt.Println("Golang: retval =", retval)
	fmt.Println("ASCII code of first character in return string = ", int(retval[0]))

	fmt.Println("Golang: Invoking M - RUNNORET^ZNEWTEST (RunNoRet), use linetag, param, no return")
	mrtn.SetRtnName("RunNoRet")
	retval, err = mrtn.CallMDescT(yottadb.NOTTP, nil, 0, "KEY2", "VALUE2")
	if nil != err {
		panic(fmt.Sprintf("CallMDescT() call failed: %s", err))
	}
	fmt.Println("Golang: retval =", retval)

	fmt.Println("Golang: Invoking M - PUT^ZNEWTEST, test return parameter")
	ET := "                                                        "
	mrtn.SetRtnName("FXPut")
	retval, err = mrtn.CallMDescT(yottadb.NOTTP, nil, 0, "JSON Structure", "Class", "Option", &ET)
	if nil != err {
		panic(fmt.Sprintf("CallMDescT() call failed: %s", err))
	}
	fmt.Println("Golang: retval =", retval, " ET=", ET)

}
