package main

import (
	"lang.yottadb.com/go/yottadb"
	// . "lang.yottadb.com/go/yottadb/internal/test_helpers"
	"fmt"
)

const YDB_DEL_TREE  = 1
const AryDim uint32 = 10                        // Dimension (capacity) of array of YDBBufferT struct
const SubSiz uint32 = 15                        // Max length of each subscripts

func main() {

	fmt.Println("=== KILL Command")
	deleteSimple("^ZNEWTEST")
	fmt.Println("=== SET Command")
	setSimple()
	fmt.Println("=== GET Command")
	getSimple()
	fmt.Println("=== Data Command")
	dataSimple()
	fmt.Println("=== Order Command")
	orderSimple()
	fmt.Println("=== Next Command")
	nextSimple()
	fmt.Println("=== Incremental Command")
	incrementalSimple()

}

func handleErr(err error) {
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Error : ", err, ":", errorCode)
		// panic(err)
	}
}

func deleteSimple(globalName string) {
	var key1 yottadb.KeyT
	var errStr yottadb.BufferT
	var tptoken uint64
	var err error
	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer errStr.Free()
	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, globalName)
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 0)
	handleErr(err)
	err = key1.DeleteST(tptoken, &errStr, yottadb.YDB_DEL_TREE)
	handleErr(err)
}

func setSimple() {

	var key1 yottadb.KeyT
	var buff1, errStr yottadb.BufferT
	var tptoken uint64
	var err error

	tptoken = yottadb.NOTTP

	/*
	set Global like this
	^ZNEWTEST="ROOT"
	^ZNEWTEST(1)="one level"
	^ZNEWTEST(1,1)="two level|1|1"
	^ZNEWTEST(1,2)="two level|1|2"
	^ZNEWTEST(1,3)="two level|1|3"
	^ZNEWTEST(2,1)="two level|2|1"
	^ZNEWTEST(2,2)="two level|2|2"
	^ZNEWTEST("A","KEY1","KEY2","KEY3","KEY4")=1
	^ZNEWTEST("B","KEY1","KEY2","KEY3","KEY4")=2
	^ZNEWTEST("SUM")=5
	*/

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()

	/*
		Global name length = 64 bytes
		10 Subscripts, each one has length = 64 bytes
	*/
	key1.Alloc(64, 10, 64)
	/*
		Value's length = 64 bytes
	*/
	buff1.Alloc(64)
	errStr.Alloc(yottadb.YDB_MAX_ERRORMSG)

	// ^ZNEWTEST="ROOT"
	// set key
	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 0)
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "ทดสอบ")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZNEWTEST(1)="one level"
	// set key
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "one level")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZNEWTEST(1,1)="two level|1|1"
	// set key
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "1")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "two level|1|1")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZNEWTEST(1,2)="two level|1|2"
	// set key
	//err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	//handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "2")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "two level|1|2")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	//^ZNEWTEST(1,3)="two level|1|3"
	// set key
	//err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	//handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "3")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "two level|1|3")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	//^ZNEWTEST(2,1)="two level|2|1"
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "2")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "1")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "two level|2|1")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	//^ZNEWTEST(2,2)="two level|2|2"
	// set key
	//err = key1.Subary.SetValStr(tptoken, &errStr, 0, "2")
	//handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "2")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "two level|2|2")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	//^ZNEWTEST("A","KEY1","KEY2","KEY3","KEY4")=1
	// set key
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 5)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "A")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "KEY1")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 2, "KEY2")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 3, "KEY3")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 4, "KEY4")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "1")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	//^ZNEWTEST("B","KEY1","KEY2","KEY3","KEY4")=2
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "B")
	handleErr(err)
	/*
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "KEY1")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 2, "KEY2")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 3, "KEY3")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 4, "KEY4")
	handleErr(err)
	*/
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "2")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	//^ZNEWTEST("SUM")=5
	// set key
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "SUM")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "5")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)
}

func getSimple() {
	var key1 yottadb.KeyT
	var buff1, errStr yottadb.BufferT
	var tptoken uint64
	var val1 string
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()
	/*
		Global name length = 64 bytes
		10 Subscripts, each one has length = 64 bytes
	*/
	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)
	/*
		Value's length = 64 bytes
	*/
	buff1.Alloc(64)

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 0)
	handleErr(err)
	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	fmt.Println("Root level = ", val1)

	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	handleErr(err)
	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	fmt.Println("one level = ", val1)

	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "1")
	handleErr(err)
	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	fmt.Println("two level = ", val1)
}

func dataSimple() {
	var key1 yottadb.KeyT
	var tptoken uint64
	var errStr yottadb.BufferT
	tptoken = yottadb.NOTTP
	var err error
	var dval uint32

	defer key1.Free()
	defer errStr.Free()
	/*
		Global name length = 64 bytes
		10 Subscripts, each one has length = 64 bytes
	*/
	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	// Check against a non-existent node - should return 0
	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "3")
	handleErr(err)
	dval, err = key1.DataST(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("Not Existed will get = %d\n", int(dval))

	// Check node with value but no subscripts - should be 1
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "1")
	handleErr(err)
	dval, err = key1.DataST(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("Existed will get = %d\n", int(dval))

	// Check against a subscripted node with no value but has descendants
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "2")
	handleErr(err)
	dval, err = key1.DataST(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("subscripted node with no value but has descendants will get = %d\n", int(dval))

	// Check against a subscripted node with a value and descendants
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	handleErr(err)
	dval, err = key1.DataST(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("subscripted node with a value and descendants will get = %d\n", int(dval))
}

func orderSimple() {

	var key1 yottadb.KeyT
	var buff1,errStr yottadb.BufferT
	var val1 string
	var tptoken uint64
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()
	/*
		Global name length = 64 bytes
		10 Subscripts, each one has length = 64 bytes
	*/
	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)

	// $O - forward
	fmt.Println("=== Order forward Command - first level")
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "")
	handleErr(err)

	buff1.Alloc(64)

	for true {
		err = key1.SubNextST(tptoken, &errStr, &buff1)
		if err != nil {
			error_code := yottadb.ErrorCode(err)
			if error_code == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		val1, err = buff1.ValStr(tptoken, &errStr)
		handleErr(err)
		fmt.Printf("%s\n", (val1))

		// Move to that key by setting the next node in the key
		err = key1.Subary.SetValStr(tptoken, &errStr, 0, val1)
		handleErr(err)
	}

	fmt.Println("=== Order forward Command - second level under first key = 1")
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "1")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "")
	handleErr(err)

	buff1.Alloc(64)

	for true {
		err = key1.SubNextST(tptoken, &errStr, &buff1)
		if err != nil {
			error_code := yottadb.ErrorCode(err)
			if error_code == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		val1, err = buff1.ValStr(tptoken, &errStr)
		handleErr(err)
		fmt.Printf("key under 1 is %s\n", (val1))

		// Move to that key by setting the next node in the key
		err = key1.Subary.SetValStr(tptoken, &errStr, 1, val1)
		handleErr(err)
	}

	fmt.Println("=== Order backward Command - second level under first key = 2")
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "2")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "")
	handleErr(err)

	buff1.Alloc(64)

	for true {
		err = key1.SubPrevST(tptoken, &errStr, &buff1)
		if err != nil {
			error_code := yottadb.ErrorCode(err)
			if error_code == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		val1, err = buff1.ValStr(tptoken, &errStr)
		handleErr(err)
		fmt.Printf("key under 2 is %s\n", (val1))

		// Move to that key by setting the next node in the key
		err = key1.Subary.SetValStr(tptoken, &errStr, 1, val1)
		handleErr(err)
	}
}

func nextSimple() {
	var key1 yottadb.KeyT
	var tptoken uint64
	var err error
	var sublst yottadb.BufferTArray
	var errStr yottadb.BufferT
	var arrayLen int

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer sublst.Free()
	defer errStr.Free()

	/*
		Global name length = 64 bytes
		10 Subscripts, each one has length = 64 bytes
	*/
	key1.Alloc(64, 10, 64)
	sublst.Alloc(AryDim, SubSiz)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	// err = key1.Subary.SetElemUsed(tptoken, &errStr, 0)
	// handleErr(err)

	fmt.Println("=== get next/prev Node (NodeNextE, NodePrevE) Command")
	err = sublst.SetElemUsed(tptoken, &errStr, AryDim)
	handleErr(err)
	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	handleErr(err)

	// parse BufferTArray to array of string
	arrayLen = int(sublst.ElemUsed())
	firstKey := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		firstKey[i] = subval
	}
	fmt.Println("first node of ^ZNEWTEST is ", firstKey)

	// next key
	err = key1.Subary.SetElemUsed(tptoken, &errStr, uint32(len(firstKey)))
	handleErr(err)
	for j, v := range firstKey {
		err = key1.Subary.SetValStr(tptoken, &errStr, uint32(j), v)
		// err = key1.Subary.SetValStr(tptoken, &errStr, uint32(j), v)	// equivalent result, may different in performance
		handleErr(err)
	}

	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	handleErr(err)

	// parse BufferTArray to array of string
	arrayLen = int(sublst.ElemUsed())
	secondKey := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		secondKey[i] = subval
	}
	fmt.Println("second node of ^ZNEWTEST is ", secondKey)

	// get subscripts after "A"
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "A")
	handleErr(err)

	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	handleErr(err)

	// parse BufferTArray to array of string
	arrayLen = int(sublst.ElemUsed())
	firstKeyAfterA := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		firstKeyAfterA[i] = subval
	}
	fmt.Println("first node of ^ZNEWTEST after A is ", firstKeyAfterA)

	// next of last key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "SUM")
	handleErr(err)

	// fmt.Println("before : ", int(sublst.ElemUsed()))
	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	// fmt.Println("after : ", int(sublst.ElemUsed()))
	// handleErr(err)
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Got Error : ", err, ":", errorCode, " When no next key")
		// panic(err)
	}
	// arrayLen = int(sublst.ElemUsed())
	arrayLen = 0	// if error happen, it will set to max length, which will be fix to set to 0 in version 1.26
	firstKeyAfterSUM := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		firstKeyAfterSUM[i] = subval
	}
	fmt.Println("next key of last key of ^ZNEWTEST (after SUM)is ", firstKeyAfterSUM)
}

func incrementalSimple() {
	var key1 yottadb.KeyT
	var buff1,incrval,dbval1,errStr yottadb.BufferT
	var tptoken uint64
	var val1 string
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer incrval.Free()
	defer dbval1.Free()
	defer errStr.Free()

	/*
		Global name length = 64 bytes
		10 Subscripts, each one has length = 64 bytes
	*/
	key1.Alloc(64, 10, 64)
	/*
		Value's length = 64 bytes
	*/
	buff1.Alloc(16)
	incrval.Alloc(16)
	dbval1.Alloc(16)
	errStr.Alloc(128)

	fmt.Println("=== Atomic increase Command")
	fmt.Println("Original value in ^ZNEWTEST SUM =5, want to add 2")

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "SUM")
	handleErr(err)
	err = incrval.SetValStr(tptoken, &errStr, "2")
	handleErr(err)
	err = key1.IncrST(tptoken, &errStr, &incrval, &dbval1)
	handleErr(err)

	val1, err = dbval1.ValStr(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("new value is %s\n", val1)

	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("database value is %s\n", val1)
}
