package main

import (
	"lang.yottadb.com/go/yottadb"
	"runtime"

	// . "lang.yottadb.com/go/yottadb/internal/test_helpers"
	"fmt"
)

const AryDim uint32 = 10                        // Dimension (capacity) of array of YDBBufferT struct
const SubSiz uint32 = 15                        // Max length of each subscripts

func handleErr(err error) {
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Error : ", err, ":", errorCode)
		// panic(err)
	}
}

func main() {

	fmt.Println("=== KILL Command")
	deleteSimple()
	fmt.Println("=== SET Command")
	setSimple()
	fmt.Println("=== GET Command")
	getSimple()
	fmt.Println("=== Data Command")
	dataSimple()
	fmt.Println("=== Order Command")
	orderSimple()
	fmt.Println("=== Next Command")
	nextSimple()
	fmt.Println("=== Incremental Command")
	incrementalSimple()
	fmt.Println("=== SET under TP Command")
	setSimpleTP()

}

func deleteSimple() {
	// 1. delete data in ^ZXXXTEST, while XXX = your nick name

	var key1 yottadb.KeyT
	var errStr yottadb.BufferT
	var tptoken uint64
	var err error
	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer errStr.Free()
	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	globalName := "^ZNEWTEST"
	err = key1.Varnm.SetValStr(tptoken, &errStr, globalName)
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 0)
	handleErr(err)
	err = key1.DeleteST(tptoken, &errStr, yottadb.YDB_DEL_TREE)
	handleErr(err)
}

func setSimple() {
	/*
	2. Create data in ^ZXXXTEST, while XXX = your nick name
	^ZXXXTEST("PO",1)="STEVE|ROGER|MANAGER|100000|20/02/2016"
	^ZXXXTEST("BA",2)="NATASHA|ROMANOVA|SENIOR|80000|25/08/2016"
	^ZXXXTEST("BA",3)="WANDA|MAXIMOFF|SENIOR|70000|25/06/2017"
	^ZXXXTEST("SCRUMMASTER",4)="TONY|STARK|MANAGER|100000|30/12/2015"
	^ZXXXTEST("DEV",5)="BRUCE|BANNER|SENIOR|85000|15/03/2016"
	^ZXXXTEST("DEV",6)="PETER|PARKER|JUNIOR|55000|15/04/2016"
	^ZXXXTEST("SUM")=5
	*/

	var key1 yottadb.KeyT
	var buff1, errStr yottadb.BufferT
	var tptoken uint64
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()

	key1.Alloc(64, 10, 64)
	buff1.Alloc(64)
	errStr.Alloc(128)

	// set global (variable)
	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)

	// set number of keys
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)

	// ^ZXXXTEST("PO",1)="STEVE|ROGER|MANAGER|100000|20/02/2016"
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "PO")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "1")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "STEVE|ROGER|MANAGER|100000|20/02/2016")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZXXXTEST("BA",2)="NATASHA|ROMANOVA|SENIOR|80000|25/08/2016"
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "BA")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "2")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "NATASHA|ROMANOVA|SENIOR|80000|25/08/2016")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZXXXTEST("BA",3)="WANDA|MAXIMOFF|SENIOR|70000|25/06/2017"
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "BA")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "3")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "WANDA|MAXIMOFF|SENIOR|70000|25/06/2017")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZXXXTEST("SCRUMMASTER",4)="TONY|STARK|MANAGER|100000|30/12/2015"
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "SCRUMMASTER")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "4")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "TONY|STARK|MANAGER|100000|30/12/2015")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZXXXTEST("DEV",5)="BRUCE|BANNER|SENIOR|85000|15/03/2016"
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "DEV")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "5")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "BRUCE|BANNER|SENIOR|85000|15/03/2016")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	// ^ZXXXTEST("DEV",6)="PETER|PARKER|JUNIOR|55000|15/04/2016"
	// set key
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "DEV")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "6")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "PETER|PARKER|JUNIOR|55000|15/04/2016")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

	//^ZNEWTEST("SUM")=5
	// set number of keys
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	// set key
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "SUM")
	handleErr(err)
	// set value
	err = buff1.SetValStr(tptoken, &errStr, "5")
	handleErr(err)
	// set data into database
	err = key1.SetValST(tptoken, &errStr, &buff1)
	handleErr(err)

}

func getSimple() {
	/*
	3. Get data from ^ZXXXTEST, while XXX = your nick name
	- PO, ID = 1
	- DEV, ID = 5
	- DEV, ID = 8
	and print on screen
	 */

	var key1 yottadb.KeyT
	var buff1, errStr yottadb.BufferT
	var tptoken uint64
	var val1 string
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()

	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)
	buff1.Alloc(64)

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)

	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "PO")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "1")
	handleErr(err)
	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	fmt.Println("PO, id = 1 is : ", val1)

	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "DEV")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "5")
	handleErr(err)
	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	fmt.Println("DEV, id = 5 is : ", val1)

	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "DEV")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "8")
	handleErr(err)
	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	fmt.Println("DEV, id = 8 is : ", val1)

}

func dataSimple() {
	/*
	4. check the existing of data in ^ZXXXTEST, while XXX = your nick name
	- Check against a non-existent node - should return 0 [ try PO,4 ]
	- Check node with value but no subscripts - should be 1 [ try DEV,5 ]
	- Check against a subscripted node with no value but has descendants [ try DEV ]
	// Check against a subscripted node with a value and descendants
	 */

	var key1 yottadb.KeyT
	var tptoken uint64
	var errStr yottadb.BufferT
	tptoken = yottadb.NOTTP
	var err error
	var dval uint32

	defer key1.Free()
	defer errStr.Free()

	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)

	// Check against a non-existent node - should return 0
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "PO")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "4")
	handleErr(err)
	dval, err = key1.DataST(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("Existed will get = %d\n", int(dval))

	// Check node with value but no subscripts - should be 1
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "DEV")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "5")
	handleErr(err)
	dval, err = key1.DataST(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("Existed will get = %d\n", int(dval))

	// Check against a subscripted node with no value but has descendants
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "DEV")
	handleErr(err)
	dval, err = key1.DataST(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("subscripted node with no value but has descendants will get = %d\n", int(dval))

}

func orderSimple() {
	/*
		5. Write code to breadth-traverse in ^ZXXXTEST, while XXX = your nick name
		- forward at the first level and print a value, you should get BA, DEV, PO, SCRUMMASTER, SUM in order
		- Order forward Command - second level under first key = DEV, you should get 5, 6 in order
		- Order backward Command - second level under first key = BA, you should get 3, 2 in order
	*/

	var key1 yottadb.KeyT
	var buff1,errStr yottadb.BufferT
	var val1 string
	var tptoken uint64
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()

	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)

	// $O - forward
	fmt.Println("=== Order forward Command - first level")
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "")
	handleErr(err)

	buff1.Alloc(64)

	for true {
		err = key1.SubNextST(tptoken, &errStr, &buff1)
		if err != nil {
			error_code := yottadb.ErrorCode(err)
			if error_code == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		val1, err = buff1.ValStr(tptoken, &errStr)
		handleErr(err)
		fmt.Printf("%s\n", val1)

		// Move to that key by setting the next node in the key
		err = key1.Subary.SetValStr(tptoken, &errStr, 0, val1)
		handleErr(err)
	}

	fmt.Println("=== Order forward Command - second level under first key = DEV")
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "DEV")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "")
	handleErr(err)

	buff1.Alloc(64)

	for true {
		err = key1.SubNextST(tptoken, &errStr, &buff1)
		if err != nil {
			error_code := yottadb.ErrorCode(err)
			if error_code == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		val1, err = buff1.ValStr(tptoken, &errStr)
		handleErr(err)
		fmt.Printf("key under 1 is %s\n", val1)

		// Move to that key by setting the next node in the key
		err = key1.Subary.SetValStr(tptoken, &errStr, 1, val1)
		handleErr(err)
	}

	fmt.Println("=== Order backward Command - second level under first key = BA")
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "BA")
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 1, "")
	handleErr(err)

	buff1.Alloc(64)

	for true {
		err = key1.SubPrevST(tptoken, &errStr, &buff1)
		if err != nil {
			error_code := yottadb.ErrorCode(err)
			if error_code == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		val1, err = buff1.ValStr(tptoken, &errStr)
		handleErr(err)
		fmt.Printf("key under 2 is %s\n", val1)

		// Move to that key by setting the next node in the key
		err = key1.Subary.SetValStr(tptoken, &errStr, 1, val1)
		handleErr(err)
	}
}

func nextSimple() {
	/*
	6. get next Node (NodeNextE)
	- get first node under ^ZXXXTEST, while XXX = your nick name, you should get [BA 2]
	- then get second node (node after the first node), you should get [BA 3]
	- get node after first key = "BA", you should get [BA 2]
	- try to get node after last key (first level = "SUM"
	Note : you also can get the previous node with NodePrevE function.
	 */

	var key1 yottadb.KeyT
	var tptoken uint64
	var err error
	var sublst yottadb.BufferTArray
	var errStr yottadb.BufferT
	var arrayLen int

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer sublst.Free()
	defer errStr.Free()

	key1.Alloc(64, 10, 64)
	sublst.Alloc(AryDim, SubSiz)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = sublst.SetElemUsed(tptoken, &errStr, AryDim)
	handleErr(err)

	// get first node
	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	handleErr(err)

	// parse BufferTArray to array of string
	arrayLen = int(sublst.ElemUsed())
	firstKey := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		firstKey[i] = subval
	}
	fmt.Println("first node of ^ZNEWTEST is ", firstKey)

	// get second node
	err = key1.Subary.SetElemUsed(tptoken, &errStr, uint32(len(firstKey)))
	handleErr(err)
	for j, v := range firstKey {
		err = key1.Subary.SetValStr(tptoken, &errStr, uint32(j), v)
		// err = key1.Subary.SetValStr(tptoken, &errStr, uint32(j), v)	// equivalent result, may different in performance
		handleErr(err)
	}

	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	handleErr(err)

	// parse BufferTArray to array of string
	arrayLen = int(sublst.ElemUsed())
	secondKey := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		secondKey[i] = subval
	}
	fmt.Println("second node of ^ZNEWTEST is ", secondKey)

	// first key after 'BA'
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "BA")
	handleErr(err)

	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	handleErr(err)

	// parse BufferTArray to array of string
	arrayLen = int(sublst.ElemUsed())
	firstKeyAfterBA := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		firstKeyAfterBA[i] = subval
	}
	fmt.Println("first node of ^ZNEWTEST after BA is ", firstKeyAfterBA)

	// get next of last node
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "SUM")
	handleErr(err)

	// fmt.Println("before : ", int(sublst.ElemUsed()))
	err = key1.NodeNextST(tptoken, &errStr, &sublst)
	// fmt.Println("after : ", int(sublst.ElemUsed()))
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Got Error : ", errorCode, " When no next key")
		// panic(err)
	}
	// arrayLen = int(sublst.ElemUsed())
	arrayLen = 0
	firstKeyAfterSUM := make([]string, arrayLen)
	for i := 0; arrayLen > i; i++ {
		subval, err := sublst.ValStr(tptoken, &errStr, uint32(i))
		handleErr(err)
		firstKeyAfterSUM[i] = subval
	}
	fmt.Println("next key of last key of ^ZNEWTEST (after SUM)is ", firstKeyAfterSUM)
}

func incrementalSimple() {
	/*
	7. Atomic increase value
	add 2 to value in ^ZNEWTEST("SUM"), which is set to 5
	result must be 7, also, please compare data return from incremental function with data in database
	 */

	var key1 yottadb.KeyT
	var buff1,incrval,dbval1,errStr yottadb.BufferT
	var tptoken uint64
	var val1 string
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer incrval.Free()
	defer dbval1.Free()
	defer errStr.Free()

	key1.Alloc(64, 10, 64)
	buff1.Alloc(16)
	incrval.Alloc(16)
	dbval1.Alloc(16)
	errStr.Alloc(128)

	fmt.Println("=== Atomic increase Command")
	fmt.Println("Original value in ^ZNEWTEST SUM =5, want to add 2")

	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
	handleErr(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleErr(err)
	err = key1.Subary.SetValStr(tptoken, &errStr, 0, "SUM")
	handleErr(err)
	err = incrval.SetValStr(tptoken, &errStr, "2")
	handleErr(err)
	err = key1.IncrST(tptoken, &errStr, &incrval, &dbval1)
	handleErr(err)

	val1, err = dbval1.ValStr(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("new value is %s\n", val1)

	buff1.Alloc(64)
	err = key1.ValST(tptoken, &errStr, &buff1)
	handleErr(err)
	val1, err = buff1.ValStr(tptoken, &errStr)
	handleErr(err)
	fmt.Printf("database value is %s\n", val1)
}

func setSimpleTP() {
	/*
		2. Create data in ^ZXXXTEST, while XXX = your nick name
		^ZXXXTEST("PO",7)="STEVE|ROGER|MANAGER|100000|20/02/2016"
	*/

	var key1 yottadb.KeyT
	var buff1, errStr yottadb.BufferT
	var tptoken uint64
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()

	key1.Alloc(64, 10, 64)
	buff1.Alloc(64)
	errStr.Alloc(128)

	// under TP
	err = yottadb.TpE(tptoken, &errStr, func(tptoken uint64, errstrp *yottadb.BufferT) int32 {
		// set global (variable)
		err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZNEWTEST")
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }

		// set number of keys
		err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }

		// ^ZXXXTEST("PO",7)="STEVE|ROGER|MANAGER|100000|20/02/2016"
		// set key
		err = key1.Subary.SetValStr(tptoken, &errStr, 0, "PO")
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }
		err = key1.Subary.SetValStr(tptoken, &errStr, 1, "7")
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }
		// set value
		err = buff1.SetValStr(tptoken, &errStr, "STEVE|ROGER|MANAGER|100000|20/02/2016")
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }
		// set data into database
		err = key1.SetValST(tptoken, &errStr, &buff1)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }

		return yottadb.YDB_OK
	}, "", []string{})

	fmt.Println("Done, return from TP is : ", err)
}

func handleError(err error) bool {
	if err == nil {
		return false
	}
	if ydbErr, ok := err.(*yottadb.YDBError); ok {
		switch yottadb.ErrorCode(ydbErr) {
		case yottadb.YDB_TP_RESTART:
			// If an application uses transactions, TP_RESTART must be handled inside the transaction callback;
			// it is here. For completeness, but ensure that one modifies this routine as needed, or copies bits
			// from it. A transaction must be restarted; this can happen if some other process modifies a value
			// we read before we commit the transaction.
			return true
		case yottadb.YDB_TP_ROLLBACK:
			// If an application uses transactions, TP_ROLLBACK must be handled inside the transaction callback;
			// it is here for completeness, but ensure that one modifies this routine as needed, or copies bits
			// from it. The transaction should be aborted; this can happen if a subtransaction return YDB_TP_ROLLBACK
			// This return will be a bit more situational.
			return true
		case yottadb.YDB_ERR_CALLINAFTERXIT:
			// The database engines was told to close, yet we tried to perform an operation. Either reopen the
			// database, or exit the program. Since the behavior of this depends on how your program should behave,
			// it is commented out so that a panic is raised.
			return true
		case yottadb.YDB_ERR_NODEEND:
			// This should be detected seperately, and handled by the looping function; calling a more generic error
			// checker should be done to check for other errors that can be encountered.
			panic("YDB_ERR_NODEEND encountered; this should be handled before in the code local to the subscript/node function")
		default:
			_, file, line, ok := runtime.Caller(1)
			if ok {
				panic(fmt.Sprintf("Assertion failure in %v at line %v with error (%d): %v", file, line, yottadb.ErrorCode(err), err))
			} else {
				panic(fmt.Sprintf("Assertion failure (%d): %v", yottadb.ErrorCode(err), err))
			}
		}
	} else {
		panic(err)
	}
}