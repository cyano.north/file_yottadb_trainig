package main

import (
	"fmt"
	"lang.yottadb.com/go/yottadb"
	"math/rand"
	"runtime"
	"strconv"

	"sync"
)

const maxMachine = 10
const message = 10000
const worker = 5

func deleteGlobal(globalName string) {
	var key1 yottadb.KeyT
	var errStr yottadb.BufferT
	var tptoken uint64
	var err error
	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer errStr.Free()
	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, globalName)
	handleError(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 0)
	handleError(err)
	err = key1.DeleteST(tptoken, &errStr, yottadb.YDB_DEL_TREE)
	handleError(err)
}

func main() {
	// ATM message report

	// clear database
	deleteGlobal("^ZATMCMSG")
	deleteGlobal("^ZRESULT")

	// init data
	var key1 yottadb.KeyT
	var buff1, errStr yottadb.BufferT
	var tptoken uint64
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()

	key1.Alloc(32, 2, 32)
	buff1.Alloc(64)
	errStr.Alloc(128)

	// ATM message
	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZATMCMSG")
	handleError(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleError(err)

	for i := 0; i < message; i++ {
		// set key
		err = key1.Subary.SetValStr(tptoken, &errStr, 0, strconv.Itoa(i))
		handleError(err)
		// set value
		err = buff1.SetValStr(tptoken, &errStr, getRandomMessage())
		handleError(err)
		// set data into database
		err = key1.SetValST(tptoken, &errStr, &buff1)
		handleError(err)
	}

	// Concurrent count score from all election unit
	var waitGroup sync.WaitGroup
	waitGroup.Add(worker)
	bucket := message / worker
	for i := 0; i < worker; i++ {
		go func (unit int) {
			start := bucket * unit
			end := start + bucket
			for j := start; j < end; j++ {
				countScore(j)
			}
			waitGroup.Done()
		} (i)
	}
	waitGroup.Wait()
	fmt.Println("Counting is Done")
}

func getRandomMessage() string {

	atmMachineCode := fmt.Sprintf("ATM%04d", rand.Intn(maxMachine))
	crflag := "DR"
	if rand.Intn(10)%2 == 0 {
		crflag = "CR"
	}
	account := "100000001"
	amount := strconv.Itoa(rand.Intn(5000))
	fee := strconv.Itoa(rand.Intn(10))

	return fmt.Sprintf("%v|%v|%v|%v|%v", atmMachineCode, crflag, account, amount, fee)
}

func countScore(key int) {
	// put your code here
	// you can use function calculate, if needed, to calculate the new result and format data

}

func calculate(crflag string, amount,fee,total,countCr,countDr,sumAmount,sumFee int) string {
	if crflag == "DR" {
		// DR
		countDr += 1
		sumAmount -= amount

	} else {
		// CR
		countCr += 1
		sumAmount += amount
	}
	total += 1
	sumFee += fee

	return fmt.Sprintf("%v|%v|%v|%v|%v", total, countCr, countDr, sumAmount,  sumFee)
}

func handleError(err error) bool {
	if err == nil {
		return false
	}
	if ydbErr, ok := err.(*yottadb.YDBError); ok {
		switch yottadb.ErrorCode(ydbErr) {
		case yottadb.YDB_TP_RESTART:
			// If an application uses transactions, TP_RESTART must be handled inside the transaction callback;
			// it is here. For completeness, but ensure that one modifies this routine as needed, or copies bits
			// from it. A transaction must be restarted; this can happen if some other process modifies a value
			// we read before we commit the transaction.
			return true
		case yottadb.YDB_TP_ROLLBACK:
			// If an application uses transactions, TP_ROLLBACK must be handled inside the transaction callback;
			// it is here for completeness, but ensure that one modifies this routine as needed, or copies bits
			// from it. The transaction should be aborted; this can happen if a subtransaction return YDB_TP_ROLLBACK
			// This return will be a bit more situational.
			return true
		case yottadb.YDB_ERR_CALLINAFTERXIT:
			// The database engines was told to close, yet we tried to perform an operation. Either reopen the
			// database, or exit the program. Since the behavior of this depends on how your program should behave,
			// it is commented out so that a panic is raised.
			return true
		case yottadb.YDB_ERR_NODEEND:
			// This should be detected seperately, and handled by the looping function; calling a more generic error
			// checker should be done to check for other errors that can be encountered.
			panic("YDB_ERR_NODEEND encountered; this should be handled before in the code local to the subscript/node function")
		default:
			_, file, line, ok := runtime.Caller(1)
			if ok {
				panic(fmt.Sprintf("Assertion failure in %v at line %v with error (%d): %v", file, line, yottadb.ErrorCode(err), err))
			} else {
				panic(fmt.Sprintf("Assertion failure (%d): %v", yottadb.ErrorCode(err), err))
			}
		}
	} else {
		panic(err)
	}
}