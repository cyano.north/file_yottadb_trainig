package main

import (
	"lang.yottadb.com/go/yottadb"
	"fmt"
	"math/rand"
	"runtime"
	"strconv"
	"sync"
	"time"

	// . "lang.yottadb.com/go/yottadb/internal/test_helpers"
	"strings"
)

const electionUnit = 20
const populationInUnit = 10000
var partyList []string
var candidateList []string

func deleteGlobal(globalName string) {
	var key1 yottadb.KeyT
	var errStr yottadb.BufferT
	var tptoken uint64
	var err error
	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer errStr.Free()
	key1.Alloc(64, 10, 64)
	errStr.Alloc(128)

	err = key1.Varnm.SetValStr(tptoken, &errStr, globalName)
	handleError(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 0)
	handleError(err)
	err = key1.DeleteST(tptoken, &errStr, yottadb.YDB_DEL_TREE)
	handleError(err)
}

func showResult() {
	var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	var party 	 string
	var winner   string
	var maxScore int
	var totalScore int

	for true {
		party, err = yottadb.SubNextE(yottadb.NOTTP, &errStr, "^ZPARTYSCORE", []string{party})
		if err != nil {
			errorCode := yottadb.ErrorCode(err)
			if errorCode == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}

		// get score
		r, err := yottadb.ValE(yottadb.NOTTP, &errStr, "^ZPARTYSCORE", []string{party})
		handleError(err)
		values := strings.Split(r, "|")
		score,_ := strconv.Atoi(values[1])
		totalScore += score
		if score > maxScore {
			maxScore = score
			winner = party
		}

		fmt.Println("Party : ", party, " get ", score)
	}

	fmt.Println("Total score is ", totalScore)
	fmt.Println("And the winner is ", winner)
}

func main() {
	rand.Seed(time.Now().UnixNano())

	// Score Counter for National Election

	// declare information
	partyList = []string {"DEMOCRATIC","REPUBLICAN","LIBERTARION","GREEN","CONSTITUTION"}
	candidateList = []string {"Hillary Clinton","Donald Trump","Gary Johnson","Jill Stein","Darrell Castle"}

	// clear database
	deleteGlobal("^ZPARTYSCORE")
	deleteGlobal("^ZUNITSCORE")

	// init data
	var key1 yottadb.KeyT
	var buff1, errStr yottadb.BufferT
	var tptoken uint64
	var err error

	tptoken = yottadb.NOTTP

	defer key1.Free()
	defer buff1.Free()
	defer errStr.Free()

	key1.Alloc(32, 2, 32)
	buff1.Alloc(64)
	errStr.Alloc(128)

	// Party total score
	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZPARTYSCORE")
	handleError(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleError(err)

	for i, party := range partyList {
		// set key
		err = key1.Subary.SetValStr(tptoken, &errStr, 0, party)
		handleError(err)
		// set value
		err = buff1.SetValStr(tptoken, &errStr, candidateList[i] + "|0")
		handleError(err)
		// set data into database
		err = key1.SetValST(tptoken, &errStr, &buff1)
		handleError(err)
	}

	// election unit score
	err = key1.Varnm.SetValStr(tptoken, &errStr, "^ZUNITSCORE")
	handleError(err)
	err = key1.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleError(err)

	for i := 0; i < electionUnit; i++ {
		// set first key
		err = key1.Subary.SetValStr(tptoken, &errStr, 0, strconv.Itoa(i))
		handleError(err)
		for _, party := range partyList {
			// set second key
			err = key1.Subary.SetValStr(tptoken, &errStr, 1, party)
			handleError(err)
			// set value
			err = buff1.SetValStr(tptoken, &errStr, "0")
			handleError(err)
			// set data into database
			err = key1.SetValST(tptoken, &errStr, &buff1)
			handleError(err)
		}
	}

	/*
	for i := 0; i < electionUnit; i++ {
		unit := strconv.Itoa(i)
		for j := 0; j < populationInUnit; j++ {
			countScore(unit)
		}
	}
	fmt.Println("Counting is Done")
	showResult()
	*/

	// Concurrent count score from all election unit
	var waitGroup sync.WaitGroup
	waitGroup.Add(electionUnit)
	for i := 0; i < electionUnit; i++ {
		go func (unit string) {
			for j := 0; j < populationInUnit; j++ {
				countScore(unit)
			}
			waitGroup.Done()
		} (strconv.Itoa(i))
	}
	waitGroup.Wait()
	fmt.Println("Counting is Done")
	showResult()

}

func countScore(unit string) {

	var keyParty, keyUnit yottadb.KeyT
	var buffParty, buffUnit, errStr yottadb.BufferT
	var tptoken uint64
	var val1 string
	var err error
	var newData string

	tptoken = yottadb.NOTTP

	defer keyParty.Free()
	defer buffParty.Free()
	defer keyUnit.Free()
	defer buffUnit.Free()
	defer errStr.Free()

	keyParty.Alloc(32, 1, 32)
	keyUnit.Alloc(32, 2, 32)
	errStr.Alloc(128)

	// random party
	party := partyList[rand.Intn(len(partyList))]

	err = keyParty.Varnm.SetValStr(tptoken, &errStr, "^ZPARTYSCORE")
	handleError(err)
	err = keyParty.Subary.SetElemUsed(tptoken, &errStr, 1)
	handleError(err)
	err = keyParty.Subary.SetValStr(tptoken, &errStr, 0, party)
	handleError(err)
	buffParty.Alloc(64)

	err = keyUnit.Varnm.SetValStr(tptoken, &errStr, "^ZUNITSCORE")
	handleError(err)
	err = keyUnit.Subary.SetElemUsed(tptoken, &errStr, 2)
	handleError(err)
	err = keyUnit.Subary.SetValStr(tptoken, &errStr, 0, unit)
	handleError(err)
	err = keyUnit.Subary.SetValStr(tptoken, &errStr, 1, party)
	handleError(err)
	buffUnit.Alloc(64)
	// time.Sleep(100 * time.Millisecond)	// to create race condition

	// under TP
	err = yottadb.TpE(tptoken, &errStr, func(tptoken uint64, errstrp *yottadb.BufferT) int32 {

		// get Party
		err = keyParty.ValST(tptoken, &errStr, &buffParty)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }
		val1, err = buffParty.ValStr(tptoken, &errStr)
		values := strings.Split(val1, "|")
		candidate := values[0]
		partyScore,_ := strconv.Atoi(values[1])

		// update party
		partyScore += 1
		// time.Sleep(300 * time.Millisecond)	// to create race condition

		// set value
		newData = candidate + "|" + strconv.Itoa(partyScore)
		err = buffParty.SetValStr(tptoken, &errStr, newData)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }

		// update data into database (Account)
		err = keyParty.SetValST(tptoken, &errStr, &buffParty)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }

		// get Unit
		err = keyUnit.ValST(tptoken, &errStr, &buffUnit)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }
		val1, err = buffUnit.ValStr(tptoken, &errStr)
		unitScore,_ := strconv.Atoi(val1)

		// update party
		unitScore += 1
		// time.Sleep(300 * time.Millisecond)	// to create race condition

		// set value
		newData = strconv.Itoa(unitScore)
		err = buffUnit.SetValStr(tptoken, &errStr, newData)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }

		// update data into database
		err = keyUnit.SetValST(tptoken, &errStr, &buffUnit)
		if handleError(err) { return int32(yottadb.ErrorCode(err)) }

		return yottadb.YDB_OK
	}, "BATCH", []string{})

}

func handleError(err error) bool {
	if err == nil {
		return false
	}
	if ydbErr, ok := err.(*yottadb.YDBError); ok {
		switch yottadb.ErrorCode(ydbErr) {
		case yottadb.YDB_TP_RESTART:
			// If an application uses transactions, TP_RESTART must be handled inside the transaction callback;
			// it is here. For completeness, but ensure that one modifies this routine as needed, or copies bits
			// from it. A transaction must be restarted; this can happen if some other process modifies a value
			// we read before we commit the transaction.
			return true
		case yottadb.YDB_TP_ROLLBACK:
			// If an application uses transactions, TP_ROLLBACK must be handled inside the transaction callback;
			// it is here for completeness, but ensure that one modifies this routine as needed, or copies bits
			// from it. The transaction should be aborted; this can happen if a subtransaction return YDB_TP_ROLLBACK
			// This return will be a bit more situational.
			return true
		case yottadb.YDB_ERR_CALLINAFTERXIT:
			// The database engines was told to close, yet we tried to perform an operation. Either reopen the
			// database, or exit the program. Since the behavior of this depends on how your program should behave,
			// it is commented out so that a panic is raised.
			return true
		case yottadb.YDB_ERR_NODEEND:
			// This should be detected seperately, and handled by the looping function; calling a more generic error
			// checker should be done to check for other errors that can be encountered.
			panic("YDB_ERR_NODEEND encountered; this should be handled before in the code local to the subscript/node function")
		default:
			_, file, line, ok := runtime.Caller(1)
			if ok {
				panic(fmt.Sprintf("Assertion failure in %v at line %v with error (%d): %v", file, line, yottadb.ErrorCode(err), err))
			} else {
				panic(fmt.Sprintf("Assertion failure (%d): %v", yottadb.ErrorCode(err), err))
			}
		}
	} else {
		panic(err)
	}
}