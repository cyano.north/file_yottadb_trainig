package main

import (
	"lang.yottadb.com/go/yottadb"
	// . "lang.yottadb.com/go/yottadb/internal/test_helpers"
	"fmt"
)

// #cgo pkg-config: yottadb
// #include "libyottadb.h"
// #include "libydberrors.h"
// int TestTpRtn_cgo(uint64_t tptoken, uintptr_t in); // Forward declaration
// void ydb_ci_t_wrapper(unsigned long tptoken, char *name, ydb_string_t *arg);
import "C"

const YDB_DEL_TREE  = 1
const AryDim uint32 = 10                        // Dimension (capacity) of array of YDBBufferT struct
const SubSiz uint32 = 15                        // Max length of each subscripts

var tptoken uint64 = yottadb.NOTTP

func handleErr(err error) {
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Error : ", err, ":",errorCode)
		// panic(err)
	}
}

func main() {

	fmt.Println("=== KILL Command")
	del()
	fmt.Println("=== SET Command")
	set()
	fmt.Println("=== GET Command")
	get()
	fmt.Println("=== Data Command")
	data()
	fmt.Println("=== Order Command")
	order()
	fmt.Println("=== Next Command")
	next()
	fmt.Println("=== Incremental Command")
	incremental()

}

func del() {
	// 1. delete data in ^ZXXXTEST, while XXX = your nick name

}

func set() {
	/*
		2. Create data in ^ZXXXTEST, while XXX = your nick name
		^ZXXXTEST("PO",1)="STEVE|ROGER|MANAGER|100000|20/02/2016"
		^ZXXXTEST("BA",2)="NATASHA|ROMANOVA|SENIOR|80000|25/08/2016"
		^ZXXXTEST("BA",3)="WANDA|MAXIMOFF|SENIOR|70000|25/06/2017"
		^ZXXXTEST("SCRUMMASTER",4)="TONY|STARK|MANAGER|100000|30/12/2015"
		^ZXXXTEST("DEV",5)="BRUCE|BANNER|SENIOR|85000|15/03/2016"
		^ZXXXTEST("DEV",6)="PETER|PARKER|JUNIOR|55000|15/04/2016"
	*/

}

func get() {
	/*
		3. Get data from ^ZXXXTEST, while XXX = your nick name
		- PO, ID = 1
		- DEV, ID = 5
		- DEV, ID = 8
		and print on screen
	*/

}

func data() {
	/*
		4. check the existing of data in ^ZXXXTEST, while XXX = your nick name
		- Check against a non-existent node - should return 0 [ try PO,4 ]
		- Check node with value but no subscripts - should be 1 [ try DEV,5 ]
		- Check against a subscripted node with no value but has descendants [ try DEV ]
		// Check against a subscripted node with a value and descendants
	*/

}

func order() {
	/*
		5. Write code to breadth-traverse in ^ZXXXTEST, while XXX = your nick name
		- forward at the first level and print a value, you should get BA, DEV, PO, SCRUMMASTER in order
		- Order forward Command - second level under first key = DEV, you should get 5, 6 in order
		- Order backward Command - second level under first key = BA, you should get 3, 2 in order
	*/

}

func next() {
	/*
		6. get next Node (NodeNextE)
		- get first node under ^ZXXXTEST, while XXX = your nick name , you should get ["BA", 2]
		- then get second node (node after the first node) , you should get ["BA", 3]
		- get node after first key = "BA" , you should get ["BA", 2]
		- try to get node after last key (first level = "SCRUMMASTER", second level ="4")
		Note : you also can get the previous node with NodePrevE function.
	*/

}

func incremental() {
	/*
		7. Atomic increase value
		add 2 to value in ^ZNEWTEST("SUM"), which is set to 5
		result must be 7, also, please compare data return from incremental function with data in database
	*/

}