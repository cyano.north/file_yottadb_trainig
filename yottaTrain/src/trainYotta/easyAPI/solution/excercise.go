package main

import (
	"lang.yottadb.com/go/yottadb"
	// . "lang.yottadb.com/go/yottadb/internal/test_helpers"
	"fmt"
)

// #cgo pkg-config: yottadb
// #include "libyottadb.h"
// #include "libydberrors.h"
// int TestTpRtn_cgo(uint64_t tptoken, uintptr_t in); // Forward declaration
// void ydb_ci_t_wrapper(unsigned long tptoken, char *name, ydb_string_t *arg);
import "C"

const YDB_DEL_TREE = 1
const AryDim uint32 = 10 // Dimension (capacity) of array of YDBBufferT struct
const SubSiz uint32 = 15 // Max length of each subscripts

var tptoken uint64 = yottadb.NOTTP

func handleErr(err error) {
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Error : ", err, ":", errorCode)
		// panic(err)
	}
}

func main() {

	fmt.Println("=== KILL Command")
	del()
	fmt.Println("=== SET Command")
	set()
	fmt.Println("=== GET Command")
	get()
	fmt.Println("=== Data Command")
	data()
	fmt.Println("=== Order Command")
	order()
	fmt.Println("=== Next Command")
	next()
	fmt.Println("=== Incremental Command")
	incremental()

}

func del() {
	// 1. delete data in ^ZXXXTEST, while XXX = your nick name

	var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// Kill
	globalName := "^ZNEWTEST"
	err = yottadb.DeleteE(tptoken, &errStr, int(C.YDB_DEL_TREE), globalName, []string{})
	handleErr(err)
}

func set() {
	/*
		2. Create data in ^ZXXXTEST, while XXX = your nick name
		^ZXXXTEST("PO",1)="STEVE|ROGER|MANAGER|100000|20/02/2016"
		^ZXXXTEST("BA",2)="NATASHA|ROMANOVA|SENIOR|80000|25/08/2016"
		^ZXXXTEST("BA",3)="WANDA|MAXIMOFF|SENIOR|70000|25/06/2017"
		^ZXXXTEST("SCRUMMASTER",4)="TONY|STARK|MANAGER|100000|30/12/2015"
		^ZXXXTEST("DEV",5)="BRUCE|BANNER|SENIOR|85000|15/03/2016"
		^ZXXXTEST("DEV",6)="PETER|PARKER|JUNIOR|55000|15/04/2016"
	*/

	var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// Set
	err = yottadb.SetValE(tptoken, &errStr, "STEVE|ROGER|MANAGER|100000|20/02/2016", "^ZNEWTEST", []string{"PO", "1"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "NATASHA|ROMANOVA|SENIOR|80000|25/08/2016", "^ZNEWTEST", []string{"BA", "2"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "WANDA|MAXIMOFF|SENIOR|70000|25/06/2017", "^ZNEWTEST", []string{"BA", "3"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "TONY|STARK|MANAGER|100000|30/12/2015", "^ZNEWTEST", []string{"SCRUMMASTER", "4"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "BRUCE|BANNER|SENIOR|85000|15/03/2016", "^ZNEWTEST", []string{"DEV", "5"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "PETER|PARKER|JUNIOR|55000|15/04/2016", "^ZNEWTEST", []string{"DEV", "6"})
	handleErr(err)
}

func get() {
	/*
		3. Get data from ^ZXXXTEST, while XXX = your nick name
		- PO, ID = 1
		- DEV, ID = 5
		- DEV, ID = 8
		and print on screen
	*/

	var err error
	var r string
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// $G
	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"PO", "1"})
	handleErr(err)
	fmt.Println("PO, id = 1 is : ", r)
	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"DEV", "5"})
	handleErr(err)
	fmt.Println("DEV, id = 5 is : ", r)
	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"DEV", "8"})
	handleErr(err)
	fmt.Println("DEV, id = 8 is : ", r)
}

func data() {
	/*
		4. check the existing of data in ^ZXXXTEST, while XXX = your nick name
		- Check against a non-existent node - should return 0 [ try PO,4 ]
		- Check node with value but no subscripts - should be 1 [ try DEV,5 ]
		- Check against a subscripted node with no value but has descendants [ try DEV ]
		// Check against a subscripted node with a value and descendants
	*/
	var err error
	var dval uint32
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// $D
	// Check against a non-existent node - should return 0
	dval, err = yottadb.DataE(tptoken, &errStr, "^ZNEWTEST", []string{"PO", "4"})
	handleErr(err)
	fmt.Printf("Not Existed will get = %d\n", int(dval))

	// Check node with value but no subscripts - should be 1
	dval, err = yottadb.DataE(tptoken, &errStr, "^ZNEWTEST", []string{"DEV", "5"})
	handleErr(err)
	fmt.Printf("Existed will get = %d\n", int(dval))

	// Check against a subscripted node with no value but has descendants
	dval, err = yottadb.DataE(tptoken, &errStr, "^ZNEWTEST", []string{"DEV"})
	handleErr(err)
	fmt.Printf("subscripted node with no value but has descendants will get = %d\n", int(dval))

}

func order() {
	/*
		5. Write code to breadth-traverse in ^ZXXXTEST, while XXX = your nick name
		- forward at the first level and print a value, you should get BA, DEV, PO, SCRUMMASTER in order
		- Order forward Command - second level under first key = DEV, you should get 5, 6 in order
		- Order backward Command - second level under first key = BA, you should get 3, 2 in order
	*/

	var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// $O - forward
	fmt.Println("=== Order forward Command - first level")
	var curKey = ""
	for true {
		curKey, err = yottadb.SubNextE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{curKey})
		if err != nil {
			errorCode := yottadb.ErrorCode(err)
			if errorCode == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		fmt.Printf("%s\n", curKey)
	}

	fmt.Println("=== Order forward Command - second level under first key = DEV")
	curKey = ""
	for true {
		curKey, err = yottadb.SubNextE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"DEV", curKey})
		if err != nil {
			errorCode := yottadb.ErrorCode(err)
			if errorCode == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		fmt.Printf("key under 'DEV' is %s\n", curKey)
	}

	fmt.Println("=== Order backward Command - second level under first key = BA")
	curKey = ""
	for true {
		curKey, err = yottadb.SubPrevE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"BA", curKey})
		if err != nil {
			errorCode := yottadb.ErrorCode(err)
			if errorCode == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		fmt.Printf("key under 'BA' is %s\n", curKey)
	}
}

func next() {
	/*
		6. get next Node (NodeNextE)
		- get first node under ^ZXXXTEST, while XXX = your nick name , you should get ["BA", 2]
		- then get second node (node after the first node) , you should get ["BA", 3]
		- get node after first key = "BA" , you should get ["BA", 2]
		- try to get node after last key (first level = "SCRUMMASTER", second level ="4")
		Note : you also can get the previous node with NodePrevE function.
	*/

	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// get first node
	firstKey, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", []string{""})
	handleErr(err)
	fmt.Println("first node of ^ZNEWTEST is ", firstKey)

	// get second node
	secondKey, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", firstKey)
	handleErr(err)
	fmt.Println("second node of ^ZNEWTEST is ", secondKey)

	// first key after 'BA'
	firstKeyAfterBA, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", []string{"BA"})
	handleErr(err)
	fmt.Println("first node of ^ZNEWTEST after BA is ", firstKeyAfterBA)

	// get next of last node
	lastKey, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", []string{"SCRUMMASTER", "4"})
	handleErr(err)
	fmt.Println("next key of last key of ^ZNEWTEST (after SCRUMMASTER, 4)is ", lastKey)

}

func incremental() {
	/*
		7. Atomic increase value
		add 2 to value in ^ZNEWTEST("SUM"), which is set to 5
		result must be 7, also, please compare data return from incremental function with data in database
	*/
	var err error
	var r string
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// Prepare data
	err = yottadb.SetValE(tptoken, &errStr, "5", "^ZNEWTEST", []string{"SUM"})
	handleErr(err)

	// incremental
	fmt.Println("=== Atomic increase Command")
	fmt.Println("Original value in ^ZNEWTEST SUM =5, want to add 2")
	var newvalB string

	newvalB, err = yottadb.IncrE(tptoken, &errStr, "2", "^ZNEWTEST", []string{"SUM"})
	handleErr(err)
	fmt.Printf("new value is %s\n", newvalB)

	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"SUM"})
	handleErr(err)
	fmt.Printf("database value is %s\n", r)
}
