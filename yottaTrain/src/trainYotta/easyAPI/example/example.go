package main

//import "C"
import (
	"encoding/json"
	"lang.yottadb.com/go/yottadb"
	// . "lang.yottadb.com/go/yottadb/internal/test_helpers"
	"fmt"
)

const YDB_DEL_TREE  = 1
const AryDim uint32 = 10                        // Dimension (capacity) of array of YDBBufferT struct
const SubSiz uint32 = 15                        // Max length of each subscripts

var tptoken uint64 = yottadb.NOTTP

func main() {

	fmt.Println("=== KILL Command")
	// TestDeleteE()
	delete("^ZNEWTEST")
	fmt.Println("=== SET Command")
	set()
	fmt.Println("=== GET Command")
	get()
	fmt.Println("=== Data Command")
	data()
	fmt.Println("=== Order Command")
	order()
	fmt.Println("=== Next Command")
	next()
	fmt.Println("=== Incremental Command")
	incremental()

}

func handleErr(err error) {
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Error : ", errorCode)
		panic(err)
	}
}

func delete(globalName string) {
	var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// $D
	// err = yottadb.DeleteE(tptoken, &errStr, YDB_DEL_TREE, globalName, []string{})
	// err = yottadb.DeleteE(tptoken, &errStr, int(C.YDB_DEL_TREE), globalName, []string{})
	err = yottadb.DeleteE(tptoken, &errStr, yottadb.YDB_DEL_TREE, globalName, []string{})
	handleErr(err)
}



func set() {
	var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)
	type test struct {
		A string `json:"a"`
		B string `json:"b"`
		C string `json:"c"`
	}
	gg := test{B: "", C: "test"}
	fmt.Println(gg)
	/*
		set Global like this
		^ZNEWTEST="ROOT"
		^ZNEWTEST(1)="one level"
		^ZNEWTEST(1,1)="two level|1|1"
		^ZNEWTEST(1,2)="two level|1|2"
		^ZNEWTEST(1,3)="two level|1|3"
		^ZNEWTEST(2,1)="two level|2|1"
		^ZNEWTEST(2,2)="two level|2|2"
		^ZNEWTEST("A","KEY1","KEY2","KEY3","KEY4")=1
		^ZNEWTEST("B","KEY1","KEY2","KEY3","KEY4")=2
		^ZNEWTEST("SUM")=5
	*/

	// $S
	err = yottadb.SetValE(tptoken, &errStr, "ROOT", "^ZNEWTEST", []string{})
	handleErr(err)

	err = yottadb.SetValE(tptoken, &errStr, "one level", "^ZNEWTEST", []string{"1"})
	handleErr(err)

	err = yottadb.SetValE(tptoken, &errStr, "two level|1|1", "^ZNEWTEST", []string{"1", "1"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "two level|1|2", "^ZNEWTEST", []string{"1", "2"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "two level|1|3", "^ZNEWTEST", []string{"1", "3"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "two level|2|1", "^ZNEWTEST", []string{"2", "1"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "two level|2|2", "^ZNEWTEST", []string{"2", "2"})
	handleErr(err)

	err = yottadb.SetValE(tptoken, &errStr, "1", "^ZNEWTEST", []string{"A","KEY1","KEY2","KEY3","KEY4"})
	handleErr(err)
	err = yottadb.SetValE(tptoken, &errStr, "2", "^ZNEWTEST", []string{"B","KEY1","KEY2","KEY3","KEY4"})
	handleErr(err)

	err = yottadb.SetValE(tptoken, &errStr, "5", "^ZNEWTEST", []string{"SUM"})
	handleErr(err)
	test01, _ := json.Marshal(gg)
	fmt.Println("test", string(test01))
	err = yottadb.SetValE(tptoken, &errStr, string(test01), "^ZTESTSTR", []string{"BLANK"})

	//err = yottadb.SetValE(tptoken, &errStr, "\"ABC\":\"VALUE\",\"DEF\":\"\",\"GHI\":\"VALUE\"", "^ZTESTSTR", []string{"BLANK"})
	handleErr(err)
}

func get() {
	var err error
	var r string
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// $G
	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{})
	handleErr(err)
	fmt.Println("Root level = ", r)
	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"1"})
	handleErr(err)
	fmt.Println("one level = ", r)
	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"1", "1"})
	handleErr(err)
	fmt.Println("two level = ", r)
	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZTESTSTR", []string{"BLANK"})
	handleErr(err)
	fmt.Println("Json String = ", r)
}

func data() {
	var err error
	var dval uint32
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// $D
	// Check against a non-existent node - should return 0
	dval, err = yottadb.DataE(tptoken, &errStr, "^ZNEWTEST", []string{"3"})
	handleErr(err)
	fmt.Printf("Not Existed will get = %d\n", int(dval))

	// Check node with value but no subscripts - should be 1
	dval, err = yottadb.DataE(tptoken, &errStr, "^ZNEWTEST", []string{"1", "1"})
	handleErr(err)
	fmt.Printf("Existed will get = %d\n", int(dval))

	// Check against a subscripted node with no value but has descendants
	dval, err = yottadb.DataE(tptoken, &errStr, "^ZNEWTEST", []string{"2"})
	handleErr(err)
	fmt.Printf("subscripted node with no value but has descendants will get = %d\n", int(dval))

	// Check against a subscripted node with a value and descendants
	dval, err = yottadb.DataE(tptoken, &errStr, "^ZNEWTEST", []string{"1"})
	handleErr(err)
	fmt.Printf("subscripted node with a value and descendants will get = %d\n", int(dval))
}

func order() {
	var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// $O - forward
	fmt.Println("=== Order forward Command - first level")
	var curKey= ""
	for true {
		curKey, err = yottadb.SubNextE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{curKey})
		if err != nil {
			errorCode := yottadb.ErrorCode(err)
			if errorCode == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		fmt.Printf("%s\n", curKey)
	}

	fmt.Println("=== Order forward Command - second level under first key = 1")
	curKey = ""
	for true {
		curKey, err = yottadb.SubNextE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"1", curKey})
		if err != nil {
			errorCode := yottadb.ErrorCode(err)
			if errorCode == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		fmt.Printf("key under 1 is %s\n", curKey)
	}

	fmt.Println("=== Order backward Command - second level under first key = 2")
	curKey = ""
	for true {
		curKey, err = yottadb.SubPrevE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"2", curKey})
		if err != nil {
			errorCode := yottadb.ErrorCode(err)
			if errorCode == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				panic(err)
			}
		}
		fmt.Printf("key under 2 is %s\n", curKey)
	}
}

func next() {
	// var err error
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// get next/prev Node
	fmt.Println("=== get next/prev Node (NodeNextE, NodePrevE) Command")

	// NodeNextE
	firstKey, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", []string{""})
	handleErr(err)
	secondKey, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", firstKey)
	handleErr(err)
	fmt.Println("first node of ^ZNEWTEST is ", firstKey)
	fmt.Println("second node of ^ZNEWTEST is ", secondKey)

	firstKeyAfterA, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", []string{"A"})
	handleErr(err)
	fmt.Println("first node of ^ZNEWTEST after A is ", firstKeyAfterA)

	lastKey, err := yottadb.NodeNextE(tptoken, &errStr, "^ZNEWTEST", []string{"SUM"})
	if err != nil {
		errorCode := yottadb.ErrorCode(err)
		fmt.Println("Got Error : ",err, ":", errorCode, " When no next key")
		// panic(err)
	}
	fmt.Println("next key of last key of ^ZNEWTEST (after SUM)is ", lastKey)

}

func incremental() {
	var err error
	var r string
	var errStr yottadb.BufferT
	defer errStr.Free()
	errStr.Alloc(128)

	// incremental
	fmt.Println("=== Atomic increase Command")
	fmt.Println("Original value in ^ZNEWTEST SUM =5, want to add 2")
	var newvalB string

	newvalB, err = yottadb.IncrE(tptoken, &errStr, "2", "^ZNEWTEST", []string{"SUM"})
	handleErr(err)
	fmt.Printf("new value is %s\n", newvalB)

	r, err = yottadb.ValE(yottadb.NOTTP, &errStr, "^ZNEWTEST", []string{"SUM"})
	handleErr(err)
	fmt.Printf("database value is %s\n", r)
}
