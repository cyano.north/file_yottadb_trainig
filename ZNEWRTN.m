NOPARAMNORET
	W !,"No param, No ret"
	Q
NOPARAMRET()
	W !,"No param, ret"
	Q "ABC"
PARAMNORET(A,B)
	W !,"param = "_$G(A)_","_$G(B)_": No ret"
	Q
PARAMRET(A,B)
	W !,"param = "_$G(A)_","_$G(B)_": ret"
	Q "ABC"
