CREATEEMP(NAM,LNAM,DEP,POS,SUPID,SAL)
	; get last employee id, +1 and save back
	N ID
	S ID=$O(^ZNEWEMP(""),-1)+1
	S ^ZNEWEMP(ID)=$G(NAM)_"|"_$G(LNAM)_"|"_$G(DEP)_"|"_$G(POS)_"|"_$G(SUPID)_"|"_$G(SAL)
	Q
	;
LISTEMP
	N ID
	S ID=""
	F  S ID=$O(^ZNEWEMP(ID)) Q:ID=""  W !,ID_":"_^ZNEWEMP(ID)
	Q
	;
LASTEMP
	N ID
	S ID=$O(^ZNEWEMP(""),-1)
	W !,ID_":"_^ZNEWEMP(ID)
	Q
	;
RAISESAL
	N ID,DATA
	S ID=""
	F  S ID=$O(^ZNEWEMP(ID)) Q:ID=""  D
	.	S DATA=^ZNEWEMP(ID)
	.	S $P(DATA,"|",6)=$P(DATA,"|",6)*1.1
	.	S ^ZNEWEMP(ID)=DATA
	Q
	;
RESIGN(ID)
	S ID=$G(ID)
	I ID="" Q
	K ^ZNEWEMP(ID)
	W !,"Employee deleted - "_ID
	Q
	;
FIND(DEP)
	N ID,DATA
	S ID=""
	F  S ID=$O(^ZNEWEMP(ID)) Q:ID=""  D
	.	S DATA=^ZNEWEMP(ID)
	.	I $P(DATA,"|",3)=$G(DEP) W !,ID_":"_^ZNEWEMP(ID)
	Q
	;

	
