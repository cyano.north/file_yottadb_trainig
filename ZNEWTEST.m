RUN(KEY,VAL)
	S ^ZNEWRT("RUN",KEY)=VAL
	Q "SUCCESS"
RUNNOPARAM()
	S ^ZNEWRT("NOPARAM")=$H
	Q "SUCCESS" 
RUNNORET(KEY,VAL)
	S ^ZNEWRT("NOTRET")=$H_"|"_KEY_"|"_VAL 
	Q  
PUT(jstr,class,option,ET)
	S ^ZNEWRT("PUT")=jstr_"|"_class_"|"_option
	S ET="Return"
	Q
TEST1(X)
	I $G(X)="" S X="A|B|C|D|EFG|JDK|XYZ"
	S P=$L(X,"|")
	I P>5 D
	.	S P1=$P(X,"|",1)
	.	S P2=$P(X,"|",2)
	.	S P3=$P(X,"|",3)
	.	S P4=$P(X,"|",4)
	.	S P5=$P(X,"|",5)
	.	F I=6:1:P S P5=P5_"!"_$P(X,"|",I)
	.	W !,P1_"|"_P2_"|"_P3_"|"_P4_"|"_P5
	E  W !,X
	Q 
